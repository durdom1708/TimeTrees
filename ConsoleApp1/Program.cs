﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace TimeTrees
{
	public struct Person
	{
		public int ID { get; }
		public string Name { get; }
		public DateTime BirthDate { get; }
		public DateTime? DeathDate { get; }
		public Person(int id, string name, DateTime birthDate, DateTime deathDate)
		{
			ID = id;
			Name = name;
			BirthDate = birthDate;
			DeathDate = deathDate;
		}
	}

	public class People
	{
		public static List<string> Names(Person[] person)
		{
			List<string> names = new List<string> ();

			for (int i = 0; i < person.Length; i++)
			{
				if (person[i].DeathDate == new DateTime())
				{
					DateTime date = person[i].BirthDate;

					int age = (int)((DateTime.Today - date).TotalDays / 365.242199);
					if (age <= 20 && DateTime.IsLeapYear(date.Year))
						names.Add(person[i].Name);
				}
			}
			return names;
		}
	}

	public struct TimelineEvent
	{
		public DateTime EventDate { get; }
		public string EventDescription { get; }
		public TimelineEvent(DateTime eventDate, string eventDescription)
		{
			EventDate = eventDate;
			EventDescription = eventDescription;
		}
	}

	public class Timeline
	{
		public static string MaxDelta(TimelineEvent[] timelines)
		{
			(DateTime minDate, DateTime maxDate) = GetMinAndMaxDate(timelines);

            (int years, int months, int days) = GetMaxDiffDates(minDate, maxDate);
			return $"{years} {months} {days}";
		}

		static (DateTime, DateTime) GetMinAndMaxDate(TimelineEvent[] timelines)
		{
			DateTime minDate = DateTime.MaxValue, maxDate = DateTime.MinValue;

			foreach (TimelineEvent timeline in timelines)
			{
				DateTime date = timeline.EventDate;

				if (minDate > date)
					minDate = date;
				if (maxDate < date)
					maxDate = date;
			}

			return (minDate, maxDate);
		}

		static (int years, int months, int days) GetMaxDiffDates(DateTime dateOne, DateTime dateTwo)
		{
			return (dateTwo.Year - dateOne.Year, dateTwo.Month - dateOne.Month, dateTwo.Day - dateOne.Day);
		}
	}

	class Program
	{
		private const int idInd = 0;
		private const int nameInd = 1;
		private const int birthDateInd = 2;
		private const int deathDateInd = 3;
		private const int timelineDateInd = 0;
		private const int timelineDescriptionInd = 1;

        static void Main(string[] args)
		{

			string timelineFile = Path.Combine(Environment.CurrentDirectory, "timelineFile.csv");
			string peopleFile = Path.Combine(Environment.CurrentDirectory, "peopleFile.csv");

			Person[] people = ParsePersonCsv(peopleFile);
			TimelineEvent[] timelines = ParseTimelineCsv(timelineFile);

			List<string> PeopleNames = People.Names(people);
			PeopleNames.ForEach(name => Console.WriteLine(name));

			Console.WriteLine(Timeline.MaxDelta(timelines));
		}

		static Person[] ParsePersonJson(string path)
		{
			string json = File.ReadAllText(path);
			return JsonConvert.DeserializeObject<Person[]>(json);
		}

		static Person[] ParsePersonCsv(string oldpath)
		{
			string[][] data = ParseText(oldpath);
			Person[] dataSplit = new Person[data.Length];

			for (int i = 0; i < data.Length; i++)
			{
				string[] elements = data[i];

				Person person = new Person(
					int.Parse(elements[idInd]),
					elements[nameInd],
					ParseDate(elements[birthDateInd]),
					elements.Length == 4 ? ParseDate(elements[deathDateInd]) : new DateTime()
					);

				dataSplit[i] = person;
			}

			return dataSplit;
		}

		static TimelineEvent[] ParseTimelineJson(string path)
		{
			string json = File.ReadAllText(path);
			return JsonConvert.DeserializeObject<TimelineEvent[]>(json);
		}

		static TimelineEvent[] ParseTimelineCsv(string oldpath)
		{
			string[][] data = ParseText(oldpath);
			TimelineEvent[] dataSplit = new TimelineEvent[data.Length];
			for (int i = 0; i < data.Length; i++)
			{
				string[] elements = data[i];

				TimelineEvent timeline = new TimelineEvent(
					ParseDate(elements[timelineDateInd]),
					elements[timelineDescriptionInd]
					);

				dataSplit[i] = timeline;
			}

			return dataSplit;
		}

		static DateTime ParseDate(string value)
		{
			DateTime date;

			if (!DateTime.TryParseExact(value, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
			{
				if (!DateTime.TryParseExact(value, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
				{
					if (!DateTime.TryParseExact(value, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
					{
						throw new Exception("Wrong Format");
					}
				}
			}

			return date;
		}

		static void WriteJsonFile(string path, object obj)
		{
			string json = JsonConvert.SerializeObject(obj);
			File.WriteAllText(path, json);
		}

		static string[][] ParseText(string path)
		{
			return Array.ConvertAll(File.ReadAllText(path, Encoding.Default).Split(new[] { '\n', '\r' },
				StringSplitOptions.RemoveEmptyEntries), arr => arr.Split(';'));
		}
	}
}
